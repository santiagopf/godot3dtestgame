
extends Spatial

# member variables here, example:
# var a=2
# var b="textvar"
var rotationSpeed = 25
var redEnemy = preload("res://enemyCube.xml")
var greenEnemy = preload("res://greenEnemy.scn")
var enemiesArray = []
var enemyCount = -1
var enemySpeed = 5
var timeElapsed = 0
var PlayerScore = 0
var enemySpawnTime = 1
var playerSpeed = 8

func _ready():
	set_process(true)
	
func _process(delta):
	get_node("Label").set_text("Score = "+str(PlayerScore))
	
	if(PlayerScore == 25):
		OS.get_main_loop().quit()
	
	if(delta > 20):
		enemySpeed += 1
	
	timeElapsed += delta
	
	if(timeElapsed > enemySpawnTime):
		CreateEnemy(randi() % 2)
		timeElapsed = 0

	for enemy in enemiesArray:
		SetCubeRotationPerFrame(delta,enemy)
		MoveEnemy(enemy, delta, enemySpeed)
		KillEnemy(enemy, get_node("PlayerCube").get_translation())
	
	SetCubeRotationPerFrame(delta, get_node("PlayerCube"))
	MovePlayerCube(delta, playerSpeed)

func SetCubeRotationPerFrame(delta, cube):
	var CubeRotation = cube.get_rotation()
	cube.set_rotation(Vector3(CubeRotation.x + 1  * delta, CubeRotation.y * delta, CubeRotation.z + 1 * delta))
	
func MovePlayerCube(delta, speed):
	var cubePosition = get_node("PlayerCube").get_translation()
	if(cubePosition.x > -8 and Input.is_action_pressed("move_left")):
		cubePosition.x -= speed * delta
	if(cubePosition.x < 8 and Input.is_action_pressed("move_right")):
		cubePosition.x += speed * delta
	if(cubePosition.y > -7 and Input.is_action_pressed("move_down")):
		cubePosition.y -= speed * delta
	if(cubePosition.y < 7 and Input.is_action_pressed("move_up")):
		cubePosition.y += speed * delta
		
	get_node("PlayerCube").set_translation(cubePosition)
	
func CreateEnemy(random):
	enemyCount += 1
	var enemy = null
	if((enemyCount%2) == 0):
		enemy = greenEnemy.instance()
		enemy.set_name("greenEnemy"+str(enemyCount))
	else:
		enemy = redEnemy.instance()
		enemy.set_name("redEnemy"+str(enemyCount))
	
	add_child(enemy)
	
	if(random < 1):
		enemy.set_translation(Vector3(randi() % 10,10,0))
	else:
		enemy.set_translation(Vector3(randi() % 10 * -1,10,0))
	
	enemiesArray.append(enemy)

func MoveEnemy(enemy, delta, enemySpeed):
	var enemyPos = enemy.get_translation()
	enemyPos.y -= enemySpeed * delta
	enemy.set_translation(enemyPos)
	
func KillEnemy(enemy, playerCubePos):
	var enemyPos = enemy.get_translation()
	if(enemyPos.y < -7 or Distance(enemyPos, playerCubePos)):
		if(Distance(enemyPos, playerCubePos)):
			SetScoreAndGameDifficulty(enemy.get_name())
			
		enemiesArray.erase(enemy)
		enemy.queue_free()

func Distance(vector1, vector2):
	var distance = sqrt(pow(vector1.x-vector2.x, 2) + pow(vector1.y-vector2.y, 2) + pow(vector1.z-vector2.z, 2))
	if(distance < 1):
		return true
	else:
		return false
	
func SetScoreAndGameDifficulty(enemyName):
	if((PlayerScore%2) == 0 and enemySpeed < 15):
		enemySpeed += 1
		if(enemySpawnTime > 0.2):
			enemySpawnTime -= 0.1
	
	if(PlayerScore == 10):
		playerSpeed += 5
		
	if(enemyName.find("greenEnemy",0) > -1):
		PlayerScore += 1
	else:
		PlayerScore -= 1

